# Pao Factory TDD project 🥖

This project is to explore the coding of a bread factory (pao).

We will cover topics including:
- git, bitbucket,
- markdown
- agile/ scrum principles
- TDD (test driven development)
- unit tests
- functional programming


Other principles:
- seperation of concerns 
- dry code

## TDD & Unit Tests

**Unit test**, is a single test that tests a part of a function.
Several tests together will help insure the functionality of the program, reduce technical debt and keep it maintainable.

Generally code lives, entropy adds complexity and short cuts lead to technical debt that can kill project.

Technical debt is the concept of taking shortcuts - like skipping documentation or not making unit tests- leading to unmanagable code.

Other factors such as time, people leaving, updates also add to technical debt.

**Test Driven Development** Is a way of developing code that is very Agile.
It's the lightest implemntation of Agile- ensures working code. 

You make the tests first, then you code the least amount of code to make the test pass.

### User Stories and tests

Good user stories can be used to make your unit tests. These are usually done by the `Three Amigos` + `Devs` + `testers` + `business analists`.

In our case it will just be us.

**User Story 1** As a bread maker, I want to provide `flour`and `water` to my `make_dough_option`, else I want `not dough`. So that I can then bake the bread.

**User Story 2** As a bread maker, I want to be able to take `dough` and use the `bake_option` to get `Pao`. Else I should get `not Pao`. So that I can make bread.

**User Story 3** As a bread maker, I want an option of `run_factory` that will take in `flour`and `water` and give me `Pao`, else give me `not Pao`.
So I can make bread with one simple action.